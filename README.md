# Mini project: 3D Heatmap POC

Display a Digital Elevation Model (DEM) with a heatmap layer in a Three.js environment.

<img src="./images/mini-heatmap-3d.gif" />

Bruno Verchère, Cédric Périon, Ninon Broyelle

## Process

* **GeoTIFF DEM uploaded by the user**
* **Interpolation by zoom level**
* **Visualize DEM in 3D and as heatmap**
* **Render**
    
    * **When zooming, heatmap and 3D texture should have been more precise, and vice versa when dezooming**
        
        Switch object when zooming is implemented. However, we can't produce DEM with less resolution from the original DEM yet.
        We try severall ways without success as :

        * Loop on DEM data to catch data and interpolate them make the browser crash, due to too heavy image.

        * Use SimplierModifier to smooth DEM directly on Mesh :
            This solution worked partially because it only work on very small image (64*64 pixels). Otherwise program takes too long time to process.
            Below we can see one of this partial result visualized on the app:

            <img src="./images/resolution.JPG" width="400" height="200"/>

            The DEM below has high resolution, and the one above is the same DEM with less resolution.

            Source : https://github.com/mrdoob/three.js/blob/dev/examples/js/modifiers/SimplifyModifier.js

        * A further track may be using d3 to reduce DEM resolution.

    * **GeoTIFF display with pixel value in Z 3D and heatmap depending on the pixel value**  
        To color the 3D map we first take all the Z values across the data set. Then we apply an linear transform so all the values fit between  0 and 1. Then, for each t ∈ [0,1] we compute a color according to a color scale using the d3-scale-chromatic module. Finaly we pass this color data to the geometry buffer so the mesh of the 3D has nice interpolated colors showing the altitude.
<img src="./images/interpolateturbo.png" />
 

## Usage

```sh
git clone https://https://gitlab.com/cperion/mini-projet-heatmap-3d.git
cd mini-projet-heatmap-3d
npm install -g parcel-bundler
npm install
parcel index.html
```

## Demonstration

The picture below is one of the DEM we used for testing. It's a GeoTIFF. Here is visualized on QGIS.

<img src="./images/dem_origin.JPG" height=200/>

Then below we can see the result when the DEM is loaded on the app. Each pixel value is seen in 3D. Each pixel is also colored to render a heatmap.

<img src="./images/heatmap_dem_3d.png" />

Then here's a demo when moving on scene :

<img src="./images/mini-heatmap-3d.gif" />

### Real-time demonstration

**You can find a real-time demonstration [here](https://cperion.gitlab.io/mini-projet-heatmap-3d/).**

A geoTIFF file is available on ./examples/mnt_alti.tif

## Resources

1. [DEM visualization with Three.js](https://github.com/zhunor/threejs-dem-visualizer/blob/master/src/js/index.js)
2. [GeoTIFF handler in Javascript](https://geotiffjs.github.io/geotiff.js/global.html)
3. [Visualizing a mountain using Three.js, Landsat and SRTM](https://medium.com/@zubazor/visualizing-a-mountain-using-three-js-landsat-and-srtm-26275c920e34)
4. [Parcel Bundler documentation](https://parceljs.org/getting_started.html)
