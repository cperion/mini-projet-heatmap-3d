import 'regenerator-runtime/runtime'
import * as THREE from 'three';
import * as GeoTIFF from "geotiff";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { SimplifyModifier } from 'three/examples/jsm/modifiers/SimplifyModifier.js';
import {hexToRgb, arrayMax, arrayMin, normaliseArray, simplify_10}  from './utils'
import { max } from 'd3';

/**
 * Programm to display DEM in 3D with heatmap visualization.
 * 
 * source to display dem with three.js : https://medium.com/@zubazor/visualizing-a-mountain-using-three-js-landsat-and-srtm-26275c920e34
 */

let scene = new THREE.Scene();
let camera = new THREE.PerspectiveCamera( 30, window.innerWidth/window.innerHeight, 0.1, 1000000 );
let dem = [];

let renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

/**
 * Controls
 */
const controls = new OrbitControls( camera, renderer.domElement );
controls.addEventListener("change", function(){
	let zoom = controls.target.distanceTo( controls.object.position );
	let currentDEM = dem.filter((elem) => elem.zoomRange[0] <= zoom && elem.zoomRange[1] > zoom);
	if(! (! typeof currentDEM !== 'undefined' || !currentDEM.geometry.uuid != mountain.uuid)){
		// The DEM change only when the resolution have to change
		mountain = currentDEM.geometry;
	}
})

/**
 * Read and display DEM
 */
let tifImage = null;
let image = null;
let geometry = null;
let mountain = null;

const inputs = document.getElementById('files');
inputs.onchange = async function() {
	GeoTIFF.fromBlob(inputs.files[0]).then((tiff) => {
		tiff.getImage().then( (tif) =>{
			tifImage = tif;
			image = {width: tifImage.getWidth(), height: tifImage.getHeight()};

			// Our initial plane geometry
			geometry = new THREE.PlaneGeometry(image.width, image.height, image.width - 1, image.height - 1);

			// Read image pixel values that each pixel corresponding a height
			tifImage.readRasters({ interleave: true }).then((data) => {

				// Fill z values of the geometry
				console.time("parseGeom");				
				for (let i = 0; i < geometry.attributes.position.count; i++) {
					if (data[i] <= -9000){
						if (i != 0){
							
							data[i] = data[i-1];
						}
						else {
							data[i] = 0;
						}
					}
					geometry.attributes.position.setZ(i, (data[i] / 20) * -1);
				}

				// Color each pixel
				let colors= []
				let normalised_data = normaliseArray(data)

				normalised_data.forEach((el) => {
					colors.push(...d3.interpolateTurbo(el)
					.substr(4) // 'revomes "rgb("
					.slice(0, -1) // remove lats ")"
					.split(',') // split !
					.map(el => el/255)) // we want r g and b between 0 and 1
				})
				geometry.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3))

				console.timeEnd("parseGeom")

				//Add mesh to scene
				const material = new THREE.MeshBasicMaterial({vertexColors: true, side: THREE.DoubleSide})
				mountain = new THREE.Mesh(geometry, material)
				mountain.rotation.x = Math.PI / 2;
				mountain.position.y = -100;

				//Fill each element of dem list for zoom level
				//As there is not several resolution, each object rendered original object.
				let dem1 = {
					geometry: mountain,
					zoomLevel: 1,
					zoomRange: [0, 100]
				}
				dem.push(dem1)

				let dem2 = {
					geometry: mountain,
					zoomLevel: 2,
					zoomRange: [100, 250]
				}

				dem.push(dem2)

				let dem3 = {
					geometry: mountain,
					zoomLevel: 3,
					zoomRange: [250, 750]
				}
				dem.push(dem3)

				let dem4 = {
					geometry: mountain,
					zoomLevel: 4,
					zoomRange: [750, 2000]
				}
				dem.push(dem4)

				scene.add(dem1.geometry);
			});
		});
	})

}

/**
 * Light
 */
const light = new THREE.AmbientLight( 0xffffff,  5 );
light.position.set(0, 0, 0);
scene.add( light );

// Define camera position
camera.position.set(200, 400, 100);
controls.update();

var animate = function () {
	requestAnimationFrame( animate );

	controls.update();
	renderer.render(scene, camera);
};

animate();