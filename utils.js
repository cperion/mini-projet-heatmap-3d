import { max } from "d3";

export function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }
  

export function arrayMax(array) {
    let max = array[0]
    array.forEach((el) => {
        if (el > max) {
            max = el
        }
    })
    return max
}

export function arrayMin(array) {
    let min = array[0]
    array.forEach((el) => {
        if (el < min) {
            min = el
        }
    })
    return min
}

export function normaliseArray(array) {
    let min = arrayMin(array)
    array = array.map(el => el - min)
    let max = arrayMax(array)
    array = array.map(el => el / max)
    return array
}

export function simplify_10(array) {
    let i = 0
    let n = array.length
    let out = []
    while(10*i < n) {
        out.push(array[10*i])
    }
    return new Uint16Array(out)
}